console.log('[Portal KIds] Flappy Bird');

const sprites = new Image();
sprites.src = './public/images/sprites.png';

const canvas = document.querySelector('canvas');
const contexto = canvas.getContext('2d');

let frames = 0;
let ultimoPlacar = 0
let maiorPlacar = 0
const somHit = new Audio();
somHit.src = './public/soundefects/hit.wav';
const somVoo = new Audio();
somVoo.src = './public/soundefects/pulo.wav';
const somPonto = new Audio();
somPonto.src = './public/soundefects/ponto.wav'

// [Plano de Fundo]
const planoDeFundo = {
  spriteX: 390,
  spriteY: 0,
  largura: 275,
  altura: 204,
  x: 0,
  y: canvas.height - 204,
  desenha()  {
    contexto.fillStyle = '#70c5ce';
    contexto.fillRect(0,0, canvas.width, canvas.height)

    contexto.drawImage(
      sprites,
      this.spriteX, this.spriteY,
      this.largura, this.altura,
      this.x, this.y,
      this.largura, this.altura,
    );

    contexto.drawImage(
      sprites,
      this.spriteX, this.spriteY,
      this.largura, this.altura,
      (this.x + this.largura), this.y,
      this.largura, this.altura,
    );
  }
};

// [Chao]
function criaChao(){
  const chao = {
    spriteX: 0,
    spriteY: 610,
    largura: 224,
    altura: 112,
    x: 0,
    y: canvas.height - 112,
    atualiza(){
      const movimentoDoChao = 1;
      const repete = this.largura/2;
      const movimentacao = this.x - movimentoDoChao;

      this.x = movimentacao % repete
    },
    desenha() {
      contexto.drawImage(
        sprites,
        this.spriteX, this.spriteY,
        this.largura, this.altura,
        this.x, this.y,
        this.largura, this.altura,
      );
  
      contexto.drawImage(
        sprites,
        this.spriteX, this.spriteY,
        this.largura, this.altura,
        (this.x + this.largura), this.y,
        this.largura, this.altura,
      );
    }
  };
  return chao
}


function fazColisao(flappyBird, chao) {
  const flappyBirdy = flappyBird.y + flappyBird.altura;
  const chaoy = chao.y;

  if( flappyBirdy >= chaoy)
  {
    return true;
  }
   return false;
}

function criaFlappyBird (){
  const flappyBird = {
    spriteX: 0,
    spriteY: 0,
    largura: 33,
    altura: 24,
    x: 10,
    y: 50,
    pulo: 4.6,
    pula() {
      flappyBird.velocidade = - flappyBird.pulo;
    },
    velocidade: 0,
    gravidade: 0.25,
    atualiza()  {
      if(fazColisao(flappyBird, globais.chao)) {
          somHit.play();
          setTimeout(() =>{
            mudaParaTela(telas.INICIO);
          }, 100)
          
          return;
      }
      this.velocidade = this.velocidade + this.gravidade
      this.y = this.y + this.velocidade;
    },
    frameAtual: 0,
    atualizaFrameAtual(){
      const intervaloDeFrames = 8;
      const passaIntervalo = frames % intervaloDeFrames === 0;

      if(passaIntervalo){
      const baseIncremento = 1;
      const incremento = baseIncremento + this.frameAtual;
      const baseRepeticao = this.movimentos.length;
      
      this.frameAtual = incremento % baseRepeticao
      }
    },
    movimentos: [{
      spriteX: 0, spriteY: 0
    },{
      spriteX: 0, spriteY: 26
    },{
      spriteX: 0, spriteY: 52
    },{
      spriteX: 0, spriteY: 26
    }],
    desenha() {
      this.atualizaFrameAtual()
      
      var { spriteX, spriteY } = this.movimentos[this.frameAtual]
      contexto.drawImage(
        sprites,
        spriteX, spriteY, // Sprite X, Sprite Y
        this.largura, this.altura, // Tamanho do recorte na sprite
        this.x, this.y,
        this.largura, this.altura,
      );
    }
  };

  return flappyBird
}

const mensagemGetReady = {
  sX: 134,
  sY: 0,
  w: 174,
  h: 152,
  x: canvas.width / 2 - 174 / 2,
  y: 50,
  desenha(){
    contexto.drawImage (
      sprites,
      this.sX, this.sY,
      this.w, this.h,
      this.x, this.y,
      this.w, this.h
    );
  }
};



function criaCanos() {

   const canos = {
    largura: 52,
    altura: 400,
    chao: {
      spriteX: 0,
      spriteY: 169,
    },

    ceu: {
      spriteX: 52,
      spriteY: 169,
    },
    espaco: 80,
    desenha() {
     
      canos.pares.forEach(function(par) {
        const yRandom = par.y;
        const espacamentoEntreCanos = 100;
        //const espacamentoEntreCanos = 90;
        const canoCeuX = par.x;
        const canoCeuY = yRandom;

       
      // [Cano do ceu]
      contexto.drawImage(
         sprites,
         canos.ceu.spriteX, canos.ceu.spriteY,
         canos.largura, canos.altura,
         canoCeuX, canoCeuY,
         canos.largura, canos.altura,
      )
      // [cano do chao]
      const canoChaoX = par.x;
      const canoChaoY = canos.altura + espacamentoEntreCanos + yRandom;
      contexto.drawImage(
        sprites,
        canos.chao.spriteX, canos.chao.spriteY,
        canos.largura, canos.altura,
        canoChaoX, canoChaoY,
        canos.largura, canos.altura,
      )
      par.canoCeu = {
        x: canoCeuX,
        y: canos.altura + canoCeuY
      }
      par.canoChao = {
        x: canoChaoX,
        y: canoChaoY
      }
      if(par.x > 30 && par.x < 33){
        globais.placar.pontuacao++
        ultimoPlacar = globais.placar.pontuacao
        if(maiorPlacar < ultimoPlacar){
          maiorPlacar = ultimoPlacar
        }
        somPonto.play();
      }
    })
  },
   temColisaoComOFlappybird(par){
         const cabecaDoFlappy = globais.flappyBird.y;
         const peDoFlappy = globais.flappyBird.y + globais.flappyBird.altura;
        if(globais.flappyBird.x >= par.x){

           if(cabecaDoFlappy <= par.canoCeu.y){
              return true;
           }
           if(peDoFlappy >= par.canoChao.y){
             return true;
          }
        }
           return false;
   },
    pares: [],
    atualiza() {
      const passou100Frames = frames % 100 === 0;
      if(passou100Frames) {
        console.log('100 frames');
         canos.pares.push({
          x: canvas.width,
          y: -150 * (Math.random() + 1),
        });
      }


       canos.pares.forEach(function(par){
         
        par.x = par.x - 2;

        if(canos.temColisaoComOFlappybird(par)){
          somHit.play();
          mudaParaTela(telas.INICIO);
        }

        if(par.x + canos.largura <= 0){
          canos.pares.shift();
        }

       });
    }
  }
   return canos;
}


function criaPlacar(){
  const placar = {
    pontuacao: 0,
    maiorPontuacao: 0,
    desenha() {
      contexto.font  = '33px "VT323"';
      contexto.textAlign = 'right';
      contexto.fillStyle = 'white';
      contexto.fillText(` ${placar.pontuacao}`,canvas.width - 10, 35);
    },
    desenhaMaior(){
      contexto.font  = '33px "VT323"';
      contexto.textAlign = 'right';
      contexto.fillStyle = 'black';
      contexto.fillText(` Maior pontuação:${placar.maiorPontuacao}`,canvas.width - 10, 420);
    }
  }
  return placar;
}


// [Telas]


var telaAtiva = {}
const globais = {}

function mudaParaTela(novaTela){
  this.telaAtiva = novaTela;
  if(telaAtiva.inicializa){
    telaAtiva.inicializa();
  }
};

const telas = {
  INICIO: {
    inicializa(){
      globais.chao = criaChao();
      globais.flappyBird = criaFlappyBird();
      globais.canos = criaCanos();
      globais.placar = criaPlacar();
      globais.placar.maiorPontuacao = maiorPlacar
      globais.placar.pontuacao = ultimoPlacar
    },
    desenha()  {
      planoDeFundo.desenha();
      globais.flappyBird.desenha();
      
      globais.chao.desenha();
      mensagemGetReady.desenha();
      globais.placar.desenha();
      globais.placar.desenhaMaior();

    },
    click()  {
      mudaParaTela(telas.JOGO);
    },
    atualiza() {
      globais.chao.atualiza();
      
    }
  },
  JOGO: {
    inicializa: () => {
       globais.placar = criaPlacar();
       globais.placar.maiorPontuacao = maiorPlacar
    },
    desenha: () =>{
      planoDeFundo.desenha();
      globais.canos.desenha();
      globais.chao.desenha();
      globais.flappyBird.desenha();
      globais.placar.desenha();
      globais.placar.desenhaMaior();
    },
    click: () =>{
      globais.flappyBird.pula();
      somVoo.play();
    },
    atualiza: () =>{
      globais.canos.atualiza();
      globais.chao.atualiza();
      globais.flappyBird.atualiza();
    },
  }
};





function loop() {
  telaAtiva.desenha();
  telaAtiva.atualiza();

  frames++;
  requestAnimationFrame(loop);
}


window.addEventListener('click', function () {
  if(telaAtiva)
      telaAtiva.click();
})

mudaParaTela(telas.INICIO)
loop();